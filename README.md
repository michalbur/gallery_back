## Prosta galeria - Prototyp

Backend galerii oparty na PHP Symfony. Do nauki.

### Do zrobienia
- [ ] - logowanie i rejestracja użytkowników
- [ ] - tworzenie miniatur obrazów
- [ ] - dodanie znaku wodnego do obrazów
- [ ] - publiczne i prywatne obrazy 

### Ważne polecenia
- `php bin/console doctrine:database:create`
- `php bin/console make:entity`
- `php bin/console make:migration`
- `php bin/console doctrine:migrations:migrate`