<?php

namespace App\Controller;

use App\Entity\Image;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Exception;

class DefaultController extends AbstractController
{
    private $goodExts = array("jpeg", "png", "gif", "jpg", "tiff", "tif", "webp");

    public function number($max)
    {
        $number = random_int(0, $max);

        return new Response(
            '<html><body>Lucky number: ' . $number . '</body></html>'
        );
    }

    public function getImages()
    {
        $products = $this->getDoctrine()->getRepository(Image::class)->findAll();
        $response = new JsonResponse($products, 200);
        $response->headers->set("Access-Control-Allow-Origin", "*");
        return $response;
    }

    public function uploadAction(Request $request)
    {
        try {
            $file = $request->files->get('my_file');
            $filename_r = $request->get("fileName");
            $watermark = $request->get("watermark");
            if ($file == null) {
                $array = array('status' => -1, 'msg' => "Empty file");
                $response = new JsonResponse($array, 400);
                $response->headers->set("Access-Control-Allow-Origin", "*");
                return $response;
            }
            $fileExtension = $file->guessExtension();
            if (!in_array($fileExtension, $this->goodExts))
            {
                $array = array('status' => -3, 'msg' => "Bad file extension");
                $response = new JsonResponse($array, 400);
                $response->headers->set("Access-Control-Allow-Origin", "*");
                return $response;
            }
            $fileName = md5(uniqid()) . '.' . $fileExtension;
            $original_name = $file->getClientOriginalName();
            $file->move('assets', $fileName);
            // $file_entity = new UploadedFile ();
            // $file_entity->setFileName ( $fileName );
            // $file_entity->setActualName ( $original_name );
            // $file_entity->setCreated ( new \DateTime () );

            // $manager = $this->getDoctrine ()->getManager ();
            // $manager->persist ( $file_entity );
            // $manager->flush ();
            $array = array(
                'status' => 1,
                'new_file' => $fileName
            );

            $entityManager = $this->getDoctrine()->getManager();
            $image = new Image();
            $image->setFilename($filename_r);
            $image->setWatermark($watermark);
            $image->setPath("assets/" . $fileName);
            $entityManager->persist($image);
            $entityManager->flush();

            $response = new JsonResponse($array, 200);
            $response->headers->set("Access-Control-Allow-Origin", "*");
            return $response;
        } catch (Exception $e) {
            $array = array('status' => -2, "msg" => $e);
            $response = new JsonResponse($array, 400);
            $response->headers->set("Access-Control-Allow-Origin", "*");
            return $response;
        }
    }
}
